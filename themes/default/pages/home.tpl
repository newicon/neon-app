{extends "layouts/layout.tpl"}
{block "content"}
	<div class="bg-gray-100">
		<div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
			<!--
			  Background overlay, show/hide based on modal state.

			  Entering: "ease-out duration-300"
				From: "opacity-0"
				To: "opacity-100"
			  Leaving: "ease-in duration-200"
				From: "opacity-100"
				To: "opacity-0"
			-->


			<!-- This element is to trick the browser into centering the modal contents. -->
			<span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>&#8203;
			<!--
			  Modal panel, show/hide based on modal state.

			  Entering: "ease-out duration-300"
				From: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
				To: "opacity-100 translate-y-0 sm:scale-100"
			  Leaving: "ease-in duration-200"
				From: "opacity-100 translate-y-0 sm:scale-100"
				To: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
			-->
			<div class="inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-3xl sm:w-full sm:p-6" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
				<div>
					<div class="mx-auto flex items-center justify-center h-12 w-12 rounded-full bg-green-100">
						<!-- Heroicon name: check -->
						<svg class="h-6 w-6 text-green-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
						</svg>
					</div>

					<div class="mt-3 text-center sm:mt-5">
						<h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-headline">
							Neon successfully installed
						</h3>
						<div class="mt-2 prose mx-auto">
							<h1>Home Page</h1>
							<p>Welcome to the home page of your neon application. This file is <code>/themes/default/pages/home.tpl</code> and it extends the <code>/theme/default/layouts/layout.tpl</code> file.</p>
							<p>Cobe cms has rendered this page for you because there are entries in the database tables <code>cms_page</code> and <code>cms_url</code> the home page is defined by the url of <code>'/'</code>. These were created from the migration <code>/apps/main/migrations/m20200101_000000_main_create_home_page.php</code> during the installation of this project. Cobe renders any pages or partials defined in these tables and stored in the themes directories automatically.<p>
							<p>Understand how this works, and then you can have fun creating your neon application.</p>
							<p class="pb-4"><svg class="h-5 mx-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1004 190"><defs/><path fill="currentColor" d="M121.4 55.7c8.4 9.8 12.6 24.5 12.6 44.2v87.8H97.4V102c0-11.1-2-19.2-6.3-24.3-4.2-5-10.8-7.6-19.7-7.6-10.6 0-19 3.4-25.3 10a37 37 0 00-9.5 26.6v81H0v-143h35.7V66a49.4 49.4 0 0119.6-18.4A59 59 0 0183 41c17.1 0 30 4.9 38.3 14.6zm167.9 67h-97.4c1.2 13.3 5.2 23.1 12 29.6 6.8 6.4 16.6 9.6 29.3 9.6a69 69 0 0043.2-15.2L287 172a77 77 0 01-25 13.1c-9.8 3.3-20 5-30.3 5-23.4 0-41.9-6.6-55.4-19.9-13.6-13.3-20.3-31.4-20.3-54.4 0-14.6 2.9-27.6 8.7-39a63.8 63.8 0 0124.8-26.2 69.9 69.9 0 0136-9.4c19.8 0 35.3 6.3 46.7 19 11.4 12.7 17.2 30.1 17.2 52.4l-.1 10.2zm-86.4-46.5c-6 6.3-9.7 15.2-11 27H258c-.4-12-3.3-21-8.8-27.1a29 29 0 00-22.8-9.3 31 31 0 00-23.5 9.4zm293.6-31.6H529l-49.1 143H448l-35.1-92-34.2 92h-31.6l-46.9-143H333l31.8 99.5 36.5-99.4h26.4l36.5 100zm60-44.5h12.1v12.1h-12V.2zm0 187V44.6h12.2v142.5h-12.3zm76-7.1a60.2 60.2 0 01-22.7-25.7 87.5 87.5 0 01-8-38.4c0-14.6 2.7-27.5 8-38.7A60.7 60.7 0 01667 42a73 73 0 0127.2 5.1c8 3 15.2 7.8 21 13.9l-5.8 10.5a86 86 0 00-21-13.6 53.2 53.2 0 00-21-4.2c-16.3 0-29 5.5-38.4 16.7-9.4 11-14 26.4-14 45.9 0 19.1 4.6 34 13.9 44.7 9.3 10.8 22 16.1 38.1 16.1 7.3 0 14.5-1.4 21.2-4a80.7 80.7 0 0021.2-13.5l5.9 10.2a57.8 57.8 0 01-21.7 14.2 73.8 73.8 0 01-26.9 5.1c-13 0-24.4-3-34.2-9.1zm129.9 0a59 59 0 01-22.3-25.7 91 91 0 01-7.9-39c0-14.5 2.7-27.4 8-38.5A60.1 60.1 0 01762.4 51c9.5-6 20.5-9 33-9 12.4 0 23.5 3 33 9 9.6 6 17 14.6 22.3 25.7a89 89 0 018 38.7 90 90 0 01-8 38.9 60 60 0 01-22.2 25.7c-9.6 6-20.6 9-33 9-12.5 0-23.6-3-33.1-9zm70-18.9c8.7-10.6 13-25.8 13-45.7 0-19.4-4.4-34.4-13.3-45.2A44.8 44.8 0 00795.4 54a44.9 44.9 0 00-36.7 16.2c-8.9 10.8-13.3 26-13.3 45.2 0 20 4.3 35.2 13 45.8a44.7 44.7 0 0036.7 16c16.2 0 28.7-5.4 37.3-16zM1004 95.8V187h-12.3V97c0-15-3-26-9-33-6-6.8-15.8-10.3-29.3-10.3-15 0-27 4.6-36.1 13.9a50.7 50.7 0 00-13.6 37v82.5h-12.3V45.5h12v25.4c4-9 10.8-16.6 19.4-21.5 8.7-5 18.6-7.4 29.7-7.4 34.3 0 51.5 18 51.5 53.8z"/></svg></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


{/block}
