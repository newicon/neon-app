import Vue from 'vue';
import axios from 'axios';

window.Vue = Vue;
window.axios = axios;
