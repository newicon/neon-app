const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const min = process.env.NODE_ENV === 'production' ? '.min' : '';

// compile js
mix.js('js/main.js', `assets/js/main${min}.js`);
// compile css and tailwind
mix.postCss('css/main.css', `assets/css/main${min}.css`, [
	require('tailwindcss')('./tailwind.config.js'),
	require('autoprefixer'),
])
.options({
	processCssUrls: false,
});
