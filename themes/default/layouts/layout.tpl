{$this->beginPage()}
<!doctype html>
<!--[if IE 9]><html class="ie9 no-js"><![endif]-->
<html class="no-js">

	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta charset="utf-8" />

		{* {meta_seo_tags } *}
		{* CMS meta tags are configurable in the page editor *}
		{$title = ''}
		{$description = ''}

		{*
		 *  The page editor can select fields from table data to display in meta text
		 *  to do this you must mark the page as a collection and specifiy the DDS class
		 *  currently a collection page does not do the query or data gathering for you.
		 *
		 *  {dds class="post" assign=post filter=['slug','=',$page.url]} - gets the blog post from the url
		 *
		 *  # replaces field tags in the meta string defined in the page editor with fields in the $post data
		 *  {$page.title = {replaceTags string=$page.title data=$post}}
		 *  {$page.description = {replaceTags string=$page.description data=$post}}
		 *  {$page.og_image = {replaceTags string=$page.og_image data=$post}}
		 *}
		<title>{$page.title|default:''}</title>
		<meta name="keywords" content="{$page.keywords|default:''}" />
		<meta name="description" content="{$page.description|default:$description}" />
		<meta itemprop="description" content="{$page.description|default:$description}" />
		<meta property="og:title" content="{$page.og_title|default:$page.title|default:$title}">
		<meta property="og:description" content="{$page.og_description|default:$page.description|default:$description}">
		<meta itemprop="name" content="{$page.title|default:$title}">
		<meta content="summary" name="twitter:card">
		<meta property="og:site_name" content="{$page.og_title|default:$page.title|default:$title}">
		<meta name="twitter:title" content="{$page.og_title|default:$page.title|default:$title}">
		<meta name="twitter:description" content="{$page.og_description|default:$page.description|default:$description}">
		{if ($page.og_image)}
			<meta property="og:image" content="{image_src src=$page.og_image}">
			<meta itemprop="image" content="{image_src src=$page.og_image}">
			<meta property="og:image" content="{image_src src=$page.og_image}">
			<meta property="og:image:secure_url" content="{image_src src=$page.og_image}">
			<meta name="twitter:image" content="{image_src src=$page.og_image}">
		{/if}
		<meta content="summary" name="twitter:card">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta property="og:url" content="{canonical}">
		{csrf_meta_tags}
		<link rel="canonical" href="{canonical}" />

		{favicon}
		<meta name="theme-color" content="#ffffff">

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">

		<!-- Styles -->
		<link rel="stylesheet" type="text/css" href="{asset path={(env('NEON_ENV')=='prod') ? '/css/main.min.css': '/css/main.css'}}">

		<!-- Scripts -->
		{* Google Analytics script - uncomment if required *}
		{* {googleAnalytics} *}
		{* keep this to include head scripts *}
		{$this->head()}
		{* load script that has been added via the cms settings to be included on every page *}
		{setting app='cms' name='head' default=''}
	</head>

	<body>
		{$this->beginBody()}

		<div class="antialiased">
			{navigation}
			{block "content"}
				CONTENT HERE
			{/block}
		</div>

		{* foot includes additional scripts for the page *}
		{* add additional scripts here *}
		<script src="{asset path={(env('NEON_ENV')=='prod') ? 'js/main.min.js': '/js/main.js'}}"></script>

		{* Keep this to include any body scripts and keep at the end *}
		{$this->endBody()}
	</body>

</html>
{$this->endPage()}