<?php
use \neon\core\helpers\Arr;

function smarty_block_layout_section($params, $content, Smarty_Internal_Template $tpl, &$repeat)
{
	if ($repeat) return;
	$class = Arr::get($params,'class', '');
	$pt = Arr::get($params,'pt', '');
	$pb = Arr::get($params,'pb', '');
	$style = Arr::get($params,'style', '');
	$pt = empty($pt) ? 'pt-16 sm:pt-20 md:pt-28' : $pt;
	$pb = empty($pb) ? 'pb-16 sm:pb-20 md:pb-28' : $pb;
	return "<section class=\"$class relative $pt $pb\"  style=\"$style\">$content</section>";
}
