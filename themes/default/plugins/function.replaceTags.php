<?php

/**
 * smarty_function_replaceTags
 * @return string
 */
function smarty_function_replaceTags($params) {
	$string = \neon\core\helpers\Arr::getRequired($params, 'string');
	$data = \neon\core\helpers\Arr::getRequired($params, 'data');
	$find = [];
	$replace = [];
	foreach($data as $k => $v) {
		if (is_string($v)) {
			$find[] = '{{' . $k . '}}';
			$replace[] = $v;
		}
	}
	return str_replace($find, $replace, $string);
}
