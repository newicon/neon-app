<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.slot_get.php
 * Type:     function
 * Name:     get slot data by its name
 * Useage:
 * {slot} IM DEFAULT {/slot} {slot_get}
 * // gives: IM DEFAULT
 * {slot name="panel"} IM PANEL {/slot} {slot_get name="panel"}
 * // gives: IM PANEL
 * -------------------------------------------------------------
 */

use neon\core\helpers\Html;
use neon\core\helpers\Arr;

function smarty_function_slot_get($params, Smarty_Internal_Template $tpl)
{
	global $slots;
	return Arr::pull($slots, Arr::get($params, 'name', 'default'));
}
