Plugins Folder
==============

These are php functions that can be used within cobe smarty templates, in a similar sense to
controller actions are for MVC approaches (cobe is the generic controller here). E.g. they might
make some database calls, create grids etc.

Ideally they should make a call to a service / grid etc and only make minor changes to data if required
so that we maintain fat well-tested services and components and extremely thin calling actions.

This folder must only contain php files and no template, css etc.
