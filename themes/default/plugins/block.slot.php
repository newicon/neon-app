<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     block.slot.php
 * Type:     block
 * Name:     slot
 * Purpose:  captures a text/html between {slot} {/slot} tags
 *           can the be used with the {slot_get} tag to retrieve
 * -------------------------------------------------------------
 */

use neon\core\helpers\Html;
use \neon\core\helpers\Arr;
/**
 * Similar to smarty.capture with two key differences
 * 1) use once - once the slot has been output it is cleared - this prevents any other components also using the same slot name
 *    or repeats of components getting a previous slot value - as can happen with capture
 * 2) empty by default
 */
function smarty_block_slot($params, $content, Smarty_Internal_Template $tpl, &$repeat)
{
	// Yes we use a global here... least the logic is only maintained here - even if the variable $slots does leach in global scope
	// Therefore as this is strickly a smarty thing - it makes sense to keep it isolated to smarty
	global $slots;
	// only output on the closing tag
	if (!$repeat) {
		$slots[collect($params)->get('name', 'default')] = $content;
	}
}
