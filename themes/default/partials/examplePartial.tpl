{extends "layouts/ajax.tpl"}
{block "content"}
	This is extending the ajax template and overriding the content block in that.
	Code that must appear on the page should be inside this block. Setup code that
	doesn't appear on the page can be outside this block.
{/block}