# Services in Neon
Services cover an area of the application. They have one or more interfaces defined against them,
have tests created against those interfaces and hide the complexities of how information in their
area is managed from the rest of the application.


## Service Interfaces
Services should define and implement one or more interfaces. As far as the rest of the application
is concerned, all they are allowed to know about the services are what is contained in the interfaces.

Services and their interfaces, to be useful need to be at a level of abstraction above that of a simple object's
public methods, and nothing about how the service has been implemented is allowed to leak out through
the interface.

The rest of the application can access the service through interfaces and not by creating an
implementation object directly. The app can handle the mapping between interfaces and what objects
need to be instantiated through the addition of interface getter methods.


## Service Tests
Service interfaces should be written, documented and tests written for them at the same time and
before any code is implemented. If you can't write a meaningful test for an interface, then it
is likely the interface has a conceptual issue.


## Service Implementations
It is up to the service how it implements the interfaces - they could on one object that delegates
to others, be on different objects, be somewhere in between. The details of this should not leak
back through to the rest of the application.