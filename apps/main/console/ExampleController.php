<?php
namespace main\console;
use yii\console\Controller;

/**
 * example console controller
 */
class ExampleController extends Controller
{
	/**
	 * An example controller to say hello to you
	 */
	public function actionHello($name)
	{
		echo "\nHello $name, I am a console application. I appear in available console commands when you type ./neo";
		echo "\nConsole applications are really useful for tool development and cronjobs that need to be run on the server.";
		echo "\n\n";
	}

}