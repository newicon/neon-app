# Neon Grids

Neon grids are useful for applications where users want to be able to see a list of filterable and orderable data in an Excel / Database table format.

If the grid belongs to a single table in Daedalus, you can use the DdsGrid specifying the table class type to generate all or some of it for you.

Create them in here (in appropriate folders if necessary).

## Todo
1. Create code generation templates to speed up grid creation