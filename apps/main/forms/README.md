# Forms in Neon

Forms can be created in a few ways. Using neon's Form object, or Daedalus forms (ddsForm) or Pheobe AppForms (appForm).

## Single Table Application Forms
For application forms that relate to a single table, consider using {ddsForm  ...} in your template file. These
are extremely quick to write, and cover a lot of usecases, including field order, showing a subset of
fields, setting labels, presetting values, complex select boxes, saving data, be made read only etc.

You won't need to create anything here, rather you can create them in a smarty template file.
See `/newicon/neon/neon/phoebe/plugins/smarty/function.ddsForm.php`
to understand more about how to configure a ddsForm.

The example below would create a contact form based on a
ddt_contact_form table, with a label of Contact Us, and a submit button of Send Message. This will display
and save to the database on submission as well as show a thank you message in place of the form and
could easily call a plugin to send out an email to the client. Not bad for 10+ lines of code.
```
{ddsForm
	classType='contact_form'
	label='Contact Us'
	enableAjaxValidation=true
	submitLabel='Send Message'
}
{if ($submitted)}
	Show the customer a thank you message in place of the form
	Also send out an email to the client via plugin e.g. {sendContactFormEmail uuid=$uuid}
	where $uuid is the UUID of the saved form details
{else}
	{$form->run()}
{/if}
```

Individual bits of the form can be rendered too, the same as for Neon Forms below.

## Multiple Table Application Form
For application forms that save across multiple tables, you can consider using the AppForms that
are definable in the admin area of neon. Once you've gotten your head around this, these are
powerful and similarly to the above can save or pull in from multiple tables, be versioned,
be volatile (reads live data) or non-volatile (shows what was added at the point of submission).

See `/newicon/neon/neon/phoebe/plugins/smarty/function.appForm.php` for more details and the
appForm section in neon.


## Neon Forms
The above two are layers on top of the Neon Form object (see `newicon/neon/neon/core/form/Form.php`).
In this case create forms here and include them in your templates where required e.g. using a plugin.
These are semi-based on Yii e.g. for validation. Neon forms can be configured pretty easily and displayed
using $form->run() or through rendering parts of the form as required.

These forms should probably be created in this folder and referenced where required using plugins.


All of these types of form currently use Vue.js to display

## Handwritten Forms or Javascript Forms
If you really need to, you can always create a simple form by hand using good old HTML, Javascript or
even Vue.js.

