<?php

/**
 * Here you can set up any application wide configuration that is not local to your environment
 */
return [
	'name' => 'APPLICATION NAME',
	'coreApps' => [
		'cms' => [
			'siteData' => [
				'domain' => 'DOMAIN_NAME',
				'name' => 'SITE NAME',
				'languages' => 'en',
				'favicon' => [
					'favicon' => '',
					'thumbnail' => ''
				],
				// example public menu items
				'menu' => [
					'items' => [
						['label'=>'ITEM_1', 'url'=>'URL_1'],
						['label' => 'ITEM_2', 'url' => 'URL_2', 'items' => [
							['label' => 'ITEM_3', 'url' => 'URL_3'],
						]],
					],
					'navClass' => 'menu__list',
					'containerClass' => 'menu menu--main-nav',
				],
			],
		],
		'user'=> [
			/**
			 * roles and routes
			 * ----------------
			 * set up each role, what url routes they are restricted to and the
			 * default page once logged in. Routes should be regular expressions
			 * E.g. to restrict all users with role member to any URL
			 * beginning with /member and with a post login home URL of
			 * /member/home you can use
			 *
			 * 'roles' => [
			 *   'member' => [
			 *     'label' => 'Member',
			 *     'routes' => [ '^/member/?.*' ],
			 *     'homeUrl' => '/member/home'
			 *   ]
			 * ]
			 *
			 */
			'roles' => []
		],
	],

	'components' => [
		'user' => [
			'class' => '\neon\user\services\User',
			'identityClass' => 'neon\user\models\User',
			'identityCookie' => ['name' => 'newicon', 'httpOnly' => true],
			'loginUrl' => ['user/account/login'],
			// Number of seconds the session will remain valid if the remember me option is set
			'rememberMeDuration' => 3600 * 24 * 30,
			'enableAutoLogin' => true,
		],

		/**
		 * The following provide opetions for the different ways of managing email sending.
		 * If you don't select one of these then the default method for sending email will be used.
		 */

		/**
		 * Postmark
		 * ========
		 * For transactions emails with a known senders "from" address.  Postmark will refuse to send emails
		 * where the from address is not configured explicitly inside Postmarks app.
		 * You must add a POSTMARK_SERVER_TOKEN environment variable to your env.ini during deployment
		 * or configure the environment variable on the target platform.
		 */
		/*'mailer' => [
			'class' => 'neon\core\mail\Mailer',
			'viewPath' => '@neon/core/mail',
			// send all emails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => (env('NEON_ENV') === 'dev'),
			// string the directory where the email messages are saved when [[useFileTransport]] is true.
			'fileTransportPath' => '@runtime/mail',
			'transport' => [
				'class' => \Postmark\Transport::class,
				'constructArgs' => [env('POSTMARK_SERVER_TOKEN')],
			],
		],*/

		/**
		 * Sendgrid
		 * ========
		 * Sendgrid is another popular email sending service - this can be easily integrated with neon's mailer
		 * by using swift mailers smtp transport method.
		 * You must configure a SENDGRID_PASSWORD environment variable
		 */
		/*'mailer' => [
			'class' => neon\core\mail\Mailer::class,
			// 'viewPath' => '@neon/core/mail',
			// Send all emails to a file if in dev mode
			'useFileTransport' => (env('NEON_ENV') === 'dev'),
			// string the directory where the email messages are saved when [[useFileTransport]] is true.
			'fileTransportPath' => '@runtime/mail',
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'smtp.sendgrid.net',
				'username' => 'apikey',
				'password' => env('SENDGRID_PASSWORD'),
				'port' => '587',
				'encryption' => 'tls',
			],
		],*/

		/**
		 * Mailgun
		 * ========
		 * This mail component allows mass email and also custom from addresses to be set
		 * leveraging mailgun's api - a MAILGUN_KEY environment variable will have to be added to the env.ini
		 * or as an environment variable on the target machine with the correct Mailgun api key
		 */
		/*'mailer' => [
			'class' => 'neon\core\mail\Mailer',
			'viewPath' => '@neon/core/mail',
			// send all emails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => (env('NEON_ENV') === 'dev'),
			// string the directory where the email messages are saved when [[useFileTransport]] is true.
			'fileTransportPath' => '@runtime/mail',
			'transport' => [
				'class' => \neon\core\mail\transport\MailgunTransport::class,
				'constructArgs' => [env('MAILGUN_KEY'), 'mg.newicon.net', 'api.eu.mailgun.net'],
			],
		],*/

	]
];
