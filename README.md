# Neon Application

This repository is for development of applications based on neon and Yii2

# Installation

## Required Tools

Neon uses [Composer](https://getcomposer.org) to manage dependencies.  Make sure you have composer installed.

[npm](https://docs.npmjs.com/about-npm) is also required for tailwind usage.

## Vendor Folder

The vendor will be saved in your repository once created. This is intentional
as experience has shown that user error means composer.lock is less reliable
than git in ensuring everyone has the same development environment.

Disk space is cheap especially compared to time wasted dealing with out of step
or incorrect vendor updates.

Also tracking of changes and issues caused by 3rd party updates are available
in the repo history clearly making for easier fault finding.

node_modules are not normally included in the repo as these generally are for dev
activities and not specific to the project. If they are specific and required
for the project to work properly, consider including them in the repo.


## Composer installation

Run the following command in your terminal to create a new neon project:

`composer create-project --prefer-dist newicon/neon-app my-project`


## Manual installation:

Clone this repository and run composer install.

```bash
git clone git@bitbucket.org:newicon/neon-app.git my-project
cd my-project
composer install
rm -rf .git
```

# Applications and Accessing in Code

Applications are created inside the apps folder. The default is main. Neon applications
can be accessed using `neon()->[app-name]`. For example

`neon()->dds`

to get hold of Daedalus. Methods on the main app can be accessed using

`main()->[some-method]`

because it has been added to bootstrap.php.  If you create additional apps and want those
to be accessible in a similar way, add them to the bootstrap file. This will help with
intellisense

# Templates

Neon uses tailwind css and laravel mix to compile javascript (Vue and Babel) and process css.
It uses postcss to compile tailwind css.

The laravel mix file that controls the build process is located `themes/default/webpack.mix.js`

You will have to install the node dependencies.
Goto: `themes/default`
In here run `npm install`

## For development

```
npm run dev
```

This will build the css from your tailwind.config.js and the css/main.css into `assets/css/main.css`
Note that a templates assets folder is published and available publicly.

## Automatically run the build

```
npm run watch
```

In order to automatically run the build when files are changed run

## Build for Production

```
npm run prod
```

This command builds the css and javascript and creates the coresponding *.min files.  For tailwind this will also run purge css which removes all the unused css classes.
This step often results in very small css files. Not that the production site will use the minified version.
If you forget to check in the minified version you may not see your changes when pushing live.


# TODO

1. Create a `npm run hot` command that will hot reload when template files / css / js or config files are changed.

2. Create some reusable components into themes (or base themes) so we aren't always reinventing the wheel.
